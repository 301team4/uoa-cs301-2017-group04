This folder contains the MATLAB code for our robot's movement algorithm.
A simple file description is given below.

SimulateMovement.m: This is the main script to run. By editing values inside
this script, you can change the map, starting location, and target location
for robot pathing simulation.

dfs.m: This is the depth-first search function file. It contains the code
that finds the steps needed to reach the target location by using a depth
first searching algorithm.

map_convert.m, plotmap.m, viewmap.m: These are given functions that we have not
edited. These are primarily used for the graphical representation of the map
that appears upon running "SimulateMovement.m".

map_x.txt: (The x can be replaced by a number) These are the map files that we
use within our code to indicate where walls are, and where the path is.