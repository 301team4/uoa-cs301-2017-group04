%% DFS(MAPFILE, STARTLOCATION, TARGETLOCATION): RETURNS RETMAP, RETVISITED, RETSTEPS
% This is the DEPTH FIRST SEARCH algorithm for the COMPSYS 301 robot. It
% utilises DFS to negotiate intersections. It moves in prefered directions
% until reaching an intersection, or node, whereupon it will explore all
% sub-nodes of a path, before moving onto the next path of the node.

% INPUTS:
% MAPFILE: The name of the .txt file containing the maze data.
% STARTLOCATION: The starting location of the robot.
% TARGETLOCATION: The target location the robot must navigate to.

% OUTPUTS:
% RETMAP: Given map file.
% RETVISITED: 2D Array that stores the number of times every square has
% been visited. Walls are seen as visited once by default.
% RETSTEPS: Vector of the coordinates for all the steps to reach the target
% location.
function [retmap,retvisited,retsteps] = dfs(mapfile,startlocation,targetlocation)

%% ************************** INITIALISATION ******************************
% RETMAP: Given map file.
% RETVISITED: 2D Array that stores the number of times every square has
% been visited. Walls are seen as visited once by default.
retmap = map_convert(mapfile);
retvisited = retmap;
plotmap(retmap);

% POS: Stores the coordinates of the square currently being analysed.
pos = startlocation;

% RETSTEPS: Vector of the coordinates for all the steps to reach the target
% location.
retsteps = pos;

% STEPCOUNT: Counter variable that counts the number of squares traversed
% in order to reach the target location.
stepcount = 1;

% NODECOORDS: 2D Array that stores [y-position of node, x-position of node,
% paths available at node, stepcount when first visited].
% NODEINDEX: Index variable points to latest position added to NODECOORDS,
% can also work as a size variable.
nodecoords = [0, 0, 0, 0];
nodeindex = 0;

% Figuring out sumdir for first step.
directions = [0, 0, 0, 0];
% If the square ABOVE is an open path...
if(pos(1) ~= 1)
    if(retmap(pos(1)-1, pos(2)) == 0) % UP
        directions(1) = 1;
    end
end
% If the square BELOW is an open path...
if(pos(1) ~= 15)
    if(retmap(pos(1)+1, pos(2)) == 0) % DOWN
        directions(2) = 1;
    end
end
% If the square to the LEFT is an open path...
if(pos(2) ~= 1)
    if(retmap(pos(1), pos(2)-1) == 0) % LEFT
        directions(3) = 1;
    end
end
% If the square to the RIGHT is an open path...
if(pos(2) ~= 19)
    if(retmap(pos(1), pos(2)+1) == 0) % RIGHT
        directions(4) = 1;
    end
end
% SUMDIR: Summing up the number of paths available from the current
% location.
sumdir = (directions(1)+directions(2)+directions(3)+directions(4));

% If the start location is a node, it will need at least 2 different
% paths leading from it. If the current location has already been
% recorded as a node, it will have been visited more than once, and
% does not need to be recorded again.
if(sumdir >= 2)
    % Recording the current position as a node, and updating the index.
    nodeindex = nodeindex + 1;
    nodecoords(nodeindex,:) = [pos(1), pos(2), sumdir, stepcount];
end

% Recording the first step on the map, on the visit map.
placestep(pos, stepcount);
retvisited(pos(1), pos(2)) = retvisited(pos(1), pos(2)) + 1;

%% ***************************** MAIN LOOP ********************************
% While we have not reached our desired location:
% Check for node positions.
% Move in available directions according to our preference of moving RIGHT,
% DOWN, LEFT, UP in that order.
while ~(pos(1) == targetlocation(1) && pos(2) == targetlocation(2))
    
    % UPDATEFLAG: This is a flag to check whether or not to update the
    % stepcounter, map, visit map and steps array.
    updateflag = 1;
    
    %% *********************** NODE CHECKING ******************************
    % This section of code checks whether the current position is a node or
    % not. A node is an intersection that has 3 or 4 paths leading out of
    % it.
    
    % DIRECTIONS: An array that stores true or false values, based on
    % whether or not the current square allows for a path to move upwards
    % or downwards.
    directions = [0, 0, 0, 0];
    
    % If the square ABOVE is an open path...
    if(pos(1) ~= 1)
        if(retmap(pos(1)-1, pos(2)) == 0) % UP
            directions(1) = 1;
        end
    end
    % If the square BELOW is an open path...
    if(pos(1) ~= 15)
        if(retmap(pos(1)+1, pos(2)) == 0) % DOWN
            directions(2) = 1;
        end
    end
    % If the square to the LEFT is an open path...
    if(pos(2) ~= 1)
        if(retmap(pos(1), pos(2)-1) == 0) % LEFT
            directions(3) = 1;
        end
    end
    % If the square to the RIGHT is an open path...
    if(pos(2) ~= 19)
        if(retmap(pos(1), pos(2)+1) == 0) % RIGHT
            directions(4) = 1;
        end
    end
    
    % SUMDIR: Summing up the number of paths available from the current
    % location.
    sumdir = (directions(1)+directions(2)+directions(3)+directions(4));
    
    % If the current location is a node, it will need at least 3 different
    % paths leading from it. If the current location has already been
    % recorded as a node, it will have been visited more than once, and
    % does not need to be recorded again.
    if((sumdir >= 3) && (retvisited(pos(1), pos(2)) == 1))
        % Recording the current position as a node, and updating the index.
        nodeindex = nodeindex + 1;
        nodecoords(nodeindex,:) = [pos(1), pos(2), sumdir, stepcount];
    end
    
    %% ************************** MOVEMENT ********************************
    % SUMDIR: This is a flag to check whether or not we have moved yet.
    moveFlag = 0;
    
    % If the square to the RIGHT is an open path, and has not been visited
    % before, move to it.
    if((pos(2) ~= 19)  && (moveFlag == 0))
        if((retmap(pos(1), pos(2)+1) == 0) && (retvisited(pos(1), pos(2)+1) == 0))
            pos(2) = pos(2) + 1;
            moveFlag = 1;
        end
    end
    % If the square BELOW is an open path, and has not been visited before,
    % move to it.
    if((pos(1) ~= 15)  && (moveFlag == 0))
        if((retmap(pos(1)+1, pos(2)) == 0) && (retvisited(pos(1)+1, pos(2)) == 0))
            pos(1) = pos(1) + 1;
            moveFlag = 1;
        end
    end
    % If the square to the LEFT is an open path, and has not been visited
    % before, move to it.
    if((pos(2) ~= 1)  && (moveFlag == 0))
        if((retmap(pos(1), pos(2)-1) == 0) && (retvisited(pos(1), pos(2)-1) == 0))
            pos(2) = pos(2) - 1;
            moveFlag = 1;
        end
    end
    % If the square ABOVE is an open path, and has not been visited before,
    % move to it.
    if((pos(1) ~= 1)  && (moveFlag == 0))
        if((retmap(pos(1)-1, pos(2)) == 0) && (retvisited(pos(1)-1, pos(2)) == 0))
            pos(1) = pos(1) - 1;
            moveFlag = 1;
        end
    end
    
    % If we have reached a dead end (We have not moved yet)...
    if(moveFlag == 0)
        % Turning off the updateflag.
        updateflag = 0;
        
        % Return to the previous node.
        pos = [nodecoords(nodeindex, 1), nodecoords(nodeindex, 2)];
        
        % While we have not found a new path...
        while(retvisited(pos(1), pos(2)) >= nodecoords(nodeindex, 3))
            % Delete exhausted node from vector, decrement index, return to
            % previous node.
            nodeindex = nodeindex - 1;
            nodecoords = nodecoords((1:nodeindex),:);
            pos = [nodecoords(nodeindex, 1), nodecoords(nodeindex, 2)];
        end
        
        % Resetting stepcount to value held when first visiting the node.
        % Deleting steps that will not be used to arrive at target
        % location, updating visit number of node.
        stepcount = nodecoords(nodeindex,4);
        retsteps = retsteps((1:stepcount),:);
        retvisited(pos(1), pos(2)) = retvisited(pos(1), pos(2)) + 1;
    end
    
    % If the update flag has not been reset...
    if(updateflag == 1)
        % Incrementing the current step count
        stepcount = stepcount + 1;

        % Recording the current step on the map, on the visit map, and in our steps vector
        placestep(pos, stepcount);
        retvisited(pos(1), pos(2)) = retvisited(pos(1), pos(2)) + 1;
        retsteps = [retsteps; pos];
    end
    
end

end

function placestep(position,i)
% This function will plot a insert yellow rectangle and also print a number in this rectangle. Use with plotmap/viewmap. 
position = [16-position(1) position(2)];
position=[position(2)+0.1 position(1)+0.1];
rectangle('Position',[position,0.8,0.8],'FaceColor','y');
c=sprintf('%d',i);
text(position(1)+0.2,position(2)+0.2,c,'FontSize',10);
end