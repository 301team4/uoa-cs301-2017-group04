% Clearing command screen and previous variable values.
clear
clc

% Using the DFS function to calculate path between start and target
% positions, based on mapfile specified by its filename.
mapfilename = 'map_8.txt';
start = [2,2];
target = [12,2];
[map,visitmap,steps]=dfs(mapfilename, start, target);

% Plotting the map, and the steps taken to go from start to target
% location.
plotmap(map,steps);

% The visitmap vector shows the number of times ALL squares were visited.
% Walls are considered to be visited once by default.