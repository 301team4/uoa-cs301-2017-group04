#include <stdio.h>

enum goDirection{LEFT, RIGHT, DOWN, UP};
enum goMovement {STRAIGHT, STOP, LEFT_VEER, RIGHT_VEER, LEFT_90, RIGHT_90};
volatile int turnOrder[500] = { -1 };
volatile int turnIndex = -1;

int levelOneReturnTurn(int prevOrientation, int currentOrientation) {
	switch(prevOrientation) {
		case DOWN:
			switch(currentOrientation) {
				case RIGHT:
					return LEFT_90;
				case DOWN:
					return STRAIGHT;
				case LEFT:
					return RIGHT_90;
				case UP:
					return STRAIGHT;
			}
			break;
		case RIGHT:
			switch(currentOrientation) {
				case RIGHT:
					return STRAIGHT;
				case DOWN:
					return RIGHT_90;
				case LEFT:
					return STRAIGHT;
				case UP:
					return LEFT_90;
			}
			break;
		case LEFT:
			switch(currentOrientation) {
				case RIGHT:
					return STRAIGHT;
				case DOWN:
					return LEFT_90;
				case LEFT:
					return STRAIGHT;
				case UP:
					return RIGHT_90;
			}
			break;
		case UP:
			switch(currentOrientation) {
				case RIGHT:
					return RIGHT_90;
				case DOWN:
					return STRAIGHT;
				case LEFT:
					return LEFT_90;
				case UP:
					return STRAIGHT;
			}
			break;
	}
	return STRAIGHT;
}

void levelOne() {
    int mapVisited[15][19];
    int mapData[15][19] = {
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
        {1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {1,0,1,0,1,0,1,1,1,0,1,1,1,0,1,1,1,0,1},
        {1,0,1,0,1,0,1,0,0,0,0,0,1,0,0,0,0,0,1},
        {1,0,1,0,1,0,0,0,1,1,1,0,1,0,1,1,1,0,1},
        {1,0,0,0,0,0,1,0,0,0,1,0,1,0,0,0,0,0,1},
        {1,1,1,0,1,1,1,0,1,0,1,0,1,0,1,1,1,0,1},
        {1,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,1},
        {1,1,1,0,1,0,1,0,1,1,1,0,1,0,1,0,1,1,1},
        {1,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,1},
        {1,0,1,1,1,0,1,0,1,0,1,0,1,1,1,1,1,0,1},
        {1,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,1},
        {1,0,1,1,1,1,1,1,1,0,1,1,1,1,1,0,1,1,1},
        {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
    };

    int currentPosition[2] = { 1,1 };
	int currentDirection = DOWN;

	int priorityOrder[500] = { -1 };
	int priorityIndex = 0;

	if(1) {
		int i = 0;
		for(; i < 15; i++) {
			int j = 0;
            for(; j < 19; j++) {
				if(mapData[i][j] == 1) {
					mapVisited[i][j] = -1;
				} else {
					mapVisited[i][j] = 0;
                }
			}
		}
	}

	mapVisited[currentPosition[0]][currentPosition[1]]++;
	priorityOrder[priorityIndex++] = currentDirection;

	int finishedMap = 0;
	while(!finishedMap) {

		if(1) {
			int oldDirection = currentDirection;
			int intersectionPaths = 0;

			if(1) {
				int index = 3;
				for(; index >= 0; index--) {
					switch(index) {
						case DOWN:
							if(mapVisited[currentPosition[0]+1][currentPosition[1]] == 0) {
								currentDirection = DOWN; intersectionPaths++;
							}
							break;
						case RIGHT:
							if(mapVisited[currentPosition[0]][currentPosition[1]+1] == 0) {
								currentDirection = RIGHT; intersectionPaths++;
							}
							break;
						case LEFT:
							if(mapVisited[currentPosition[0]][currentPosition[1]-1] == 0) {
								currentDirection = LEFT; intersectionPaths++;
							}
							break;
						case UP:
							if(mapVisited[currentPosition[0]-1][currentPosition[1]] == 0) {
								currentDirection = UP; intersectionPaths++;
							}
							break;
					}
				}
			}

			if((currentDirection != oldDirection) || (intersectionPaths > 1)) {
				priorityOrder[priorityIndex++] = currentDirection;
			} else if(intersectionPaths == 0) {

				int uTurning = 0;
				switch(currentDirection) {
					case DOWN:
						if((mapVisited[currentPosition[0]+1][currentPosition[1]] == -1) && (mapVisited[currentPosition[0]][currentPosition[1]-1] == -1) && (mapVisited[currentPosition[0]][currentPosition[1]+1] == -1)) {
							currentDirection = UP; uTurning++;
						}
						break;
					case RIGHT:
						if((mapVisited[currentPosition[0]][currentPosition[1]+1] == -1) && (mapVisited[currentPosition[0]-1][currentPosition[1]] == -1) && (mapVisited[currentPosition[0]+1][currentPosition[1]] == -1)) {
							currentDirection = LEFT; uTurning++;
						}
						break;
					case LEFT:
						if((mapVisited[currentPosition[0]][currentPosition[1]-1] == -1) && (mapVisited[currentPosition[0]-1][currentPosition[1]] == -1) && (mapVisited[currentPosition[0]+1][currentPosition[1]] == -1)) {
							currentDirection = RIGHT; uTurning++;
						}
						break;
					case UP:
						if((mapVisited[currentPosition[0]-1][currentPosition[1]] == -1) && (mapVisited[currentPosition[0]][currentPosition[1]-1] == -1) && (mapVisited[currentPosition[0]][currentPosition[1]+1] == -1)) {
							currentDirection = DOWN; uTurning++;
						}
						break;
					default:
						break;
				}

				if(uTurning != 0) {
					priorityOrder[priorityIndex++] = currentDirection;
				} else {

					int potentialDirection = -1;
					int minFlag = 10000;

					if(1) {
						if((mapVisited[currentPosition[0]-1][currentPosition[1]] != -1) || (mapVisited[currentPosition[0]+1][currentPosition[1]] != -1)) {
							if(mapVisited[currentPosition[0]][currentPosition[1]+1] != -1) {
								intersectionPaths++;
							}
							if(mapVisited[currentPosition[0]][currentPosition[1]-1] != -1) {
								intersectionPaths++;
							}
						}
						if((mapVisited[currentPosition[0]][currentPosition[1]-1] != -1) || (mapVisited[currentPosition[0]][currentPosition[1]+1] != -1)) {
							if(mapVisited[currentPosition[0]+1][currentPosition[1]] != -1) {
								intersectionPaths++;
							}
							if(mapVisited[currentPosition[0]-1][currentPosition[1]] != -1) {
								intersectionPaths++;
							}
						}
					}

					if(1) {
						int index = 3;
						for(; index >= 0; index--) {
							switch(index) {
								case DOWN:
									if((mapVisited[currentPosition[0]+1][currentPosition[1]] <= minFlag) && (mapVisited[currentPosition[0]+1][currentPosition[1]] != -1) && (currentDirection != UP)) {
										minFlag = mapVisited[currentPosition[0]+1][currentPosition[1]];
										potentialDirection = DOWN;
									}
									break;
								case RIGHT:
									if((mapVisited[currentPosition[0]][currentPosition[1]+1] <= minFlag) && (mapVisited[currentPosition[0]][currentPosition[1]+1] != -1) && (currentDirection != LEFT)) {
										minFlag = mapVisited[currentPosition[0]][currentPosition[1]+1];
										potentialDirection = RIGHT;
									}
									break;
								case LEFT:
									if((mapVisited[currentPosition[0]][currentPosition[1]-1] <= minFlag) && (mapVisited[currentPosition[0]][currentPosition[1]-1] != -1) && (currentDirection != RIGHT)) {
										minFlag = mapVisited[currentPosition[0]][currentPosition[1]-1];
										potentialDirection = LEFT;
									}
									break;
								case UP:
									if((mapVisited[currentPosition[0]-1][currentPosition[1]] <= minFlag) && (mapVisited[currentPosition[0]-1][currentPosition[1]] != -1) && (currentDirection != DOWN)) {
										minFlag = mapVisited[currentPosition[0]-1][currentPosition[1]];
										potentialDirection = UP;
									}
									break;
							}
						}
					}

					if((potentialDirection != oldDirection) && (potentialDirection != -1)) {
						currentDirection = potentialDirection;
						priorityOrder[priorityIndex++] = currentDirection;
					} else if(intersectionPaths != 0) {
						priorityOrder[priorityIndex++] = currentDirection;
					}

				}
			}
		}

		switch(currentDirection) {
			case DOWN:
				mapVisited[++currentPosition[0]][currentPosition[1]]++;
				break;
			case RIGHT:
				mapVisited[currentPosition[0]][++currentPosition[1]]++;
				break;
			case LEFT:
				mapVisited[currentPosition[0]][--currentPosition[1]]++;
				break;
			case UP:
				mapVisited[--currentPosition[0]][currentPosition[1]]++;
				break;
			default:
				break;
		}

		if(1) {
			finishedMap = 1;
			int i = 0;
            for(; i < 15; i++) {
				int j = 0;
                for(; j < 19; j++) {
					if(mapVisited[i][j] == 0) {
						finishedMap = 0;
						break;
					}
				}
				if(finishedMap == 0) {
					break;
				}
			}
		}

	}

	turnIndex = priorityIndex - 1;

	if(1) {

        int i = 1;
        for(; i < priorityIndex; i++) {
            turnOrder[i-1] = levelOneReturnTurn(priorityOrder[i-1],priorityOrder[i]);
        }

    }
}

int main() {
	levelOne();

	return 0;
}
