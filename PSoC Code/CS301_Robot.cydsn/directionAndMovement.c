#include <project.h>

#include "globals.h"
#include "defines.h"
#include "functions.h"

int determineDirection() {
    
    if(turnElement > turnIndex) {
        return STOP;
    }
    
    switch(turnOrder[turnElement]) {
        case LEFT_90:
            if(finishTurnFlag == 1) {
                break;
            } else if(startTurnFlag == 1) {
                if(valueADC.sLeftCentre == 0) {
                    startTurnFlag = 2;
                    CyDelay(MOVEMENT_DELAY);
                }
                return LEFT_90;
            } else if(startTurnFlag == 2) {
                if((valueADC.sRightTop == 1) && (valueADC.sCentre == 1)) {
                    goStop(); CyDelay(STOP_DELAY);
                    startTurnFlag = 3;
                    break;
                }
                return LEFT_90;
            } else if(startTurnFlag == 3) {
                if(valueADC.sLeftCentre == 0) {
                    turnElement++;
                    startTurnFlag = 0; uTurnFlag = 0; finishTurnFlag = 1;
                    Timer_NewTurn_Start(); LED_ON;
                }
                break;
            } else {
                if((valueADC.sLeftCentre == 1) && (valueADC.sCentre == 1)) {
                    goStop(); CyDelay(STOP_DELAY);
                    startTurnFlag = 1;
                    return STOP;
                }
            }
            break;
            
        case RIGHT_90:
            if(finishTurnFlag == 1) {
                break;
            } else if(startTurnFlag == 1) {
                if(valueADC.sRightCentre == 0) {
                    startTurnFlag = 2;
                    CyDelay(MOVEMENT_DELAY);
                }
                return RIGHT_90;
            } else if(startTurnFlag == 2) {
                if((valueADC.sLeftTop == 1) && (valueADC.sCentre == 1)) {
                    goStop(); CyDelay(STOP_DELAY);
                    startTurnFlag = 3;
                    break;
                }
                return RIGHT_90;
            } else if(startTurnFlag == 3) {
                if(valueADC.sRightCentre == 0) {
                    turnElement++;
                    startTurnFlag = 0; uTurnFlag = 0; finishTurnFlag = 1;
                    Timer_NewTurn_Start(); LED_ON;
                }
            } else {
                if((valueADC.sRightCentre == 1) && (valueADC.sCentre == 1)) {
                    goStop(); CyDelay(STOP_DELAY);
                    startTurnFlag = 1;
                    return STOP;
                }
            }
            break;
            
        case UTURN:
            if(finishTurnFlag == 1) {
                break;
            } else if(uTurnFlag == 1) {
                if(valueADC.sLeftTop == 1) {
                    goStop(); CyDelay(STOP_DELAY);
                    turnElement++;
                    startTurnFlag = 0; uTurnFlag = 0; finishTurnFlag = 1;
                    Timer_NewTurn_Start(); LED_ON;
                    break;
                }
                return LEFT_90;
            } else {
                if((valueADC.sRear == 1) && (valueADC.sCentre == 1) && (valueADC.sLeftCentre == 0) && (valueADC.sLeftTop == 0) && (valueADC.sRightCentre == 0) && (valueADC.sRightTop == 0)) {
                    goStop(); CyDelay(STOP_DELAY);
                    goLeft90(); CyDelay(UTURN_DELAY);
                    uTurnFlag = 1;
                    return STOP;
                } else if((valueADC.sLeftCentre == 1) && (valueADC.sCentre == 1)) {
                    goStop(); CyDelay(STOP_DELAY);
                    goLeft90(); CyDelay(UTURN_DELAY);
                    uTurnFlag = 1;
                    return STOP;
                } else if((valueADC.sRightCentre == 1) && (valueADC.sCentre == 1)) {
                    goStop(); CyDelay(STOP_DELAY);
                    goLeft90(); CyDelay(UTURN_DELAY);
                    uTurnFlag = 1;
                    return STOP;
                }
            }
            break;
        
        case STRAIGHT:
            if(finishTurnFlag == 1) {
                break;
            } else if(startTurnFlag == 1) {
                if((valueADC.sLeftCentre == 0) && (valueADC.sRightCentre == 0)) {
                    turnElement++;
                    startTurnFlag = 0; uTurnFlag = 0; finishTurnFlag = 1;
                    Timer_NewTurn_Start(); LED_ON;
                }
            } else {
                if((valueADC.sLeftCentre == 1) && (valueADC.sCentre == 1)) {
                    startTurnFlag = 1;
                } else if((valueADC.sRightCentre == 1) && (valueADC.sCentre == 1)) {
                    startTurnFlag = 1;
                }
            }
            break;
            
        default:
            return STOP;
    }
    
    if((valueADC.sLeftTop == 0) && (valueADC.sRightTop == 1)) {
        return RIGHT_VEER;
    } else if((valueADC.sLeftTop == 1) && (valueADC.sRightTop == 0)) {
        return LEFT_VEER;
    } else if((valueADC.sLeftTop == 0) && (valueADC.sRightTop == 0)) {
        return SLOW;
    } else {
        return STRAIGHT;
    }
    
    return LEFT_90;
}

/*

*/
void goStraight() {
    PWM_1_WriteCompare(PWM_VALUE_STRAIGHT);
    PWM_2_WriteCompare(PWM_VALUE_STRAIGHT);
}
void goSlow() {
    PWM_1_WriteCompare(PWM_VALUE_SLOW);
    PWM_2_WriteCompare(PWM_VALUE_SLOW);
}
void goStop() {
    PWM_1_WriteCompare(PWM_VALUE_STOP);
    PWM_2_WriteCompare(PWM_VALUE_STOP);
}
void goLeftVeer() {
    PWM_1_WriteCompare(PWM_VALUE_VEER);
    PWM_2_WriteCompare(PWM_VALUE_STRAIGHT);
}
void goRightVeer() {
    PWM_1_WriteCompare(PWM_VALUE_STRAIGHT);
    PWM_2_WriteCompare(PWM_VALUE_VEER);
}
void goLeft90() {
    PWM_1_WriteCompare(PWM_VALUE_90_REVERSE);
    PWM_2_WriteCompare(PWM_VALUE_90_FORWARD);
}
void goRight90() {
    PWM_1_WriteCompare(PWM_VALUE_90_FORWARD);
    PWM_2_WriteCompare(PWM_VALUE_90_REVERSE);
}