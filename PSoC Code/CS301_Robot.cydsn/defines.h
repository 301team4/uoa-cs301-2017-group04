//* ========================================
// ADC 
#define MAX_ADC_COUNT 50
#define ON_TRACK_THRESHHOLD 170
#define OFF_TRACK_THRESHHOLD 300

//* ========================================
// DELAY
#define MOVEMENT_DELAY 375
#define UTURN_DELAY 900
#define STOP_DELAY 500

//* ========================================
// GENERAL DEBUG
#define LED_ON          LED_Write(1)
#define LED_OFF         LED_Write(0)
#define LED_TOGGLE      LED_Write(~LED_Read())

//* ========================================
// INITIAL DIRECTION/COORDS
#define INITIAL_X 1;
#define INITIAL_Y 1;
#define INITIAL_DIRECTION DOWN;

//* ========================================
// MAP SPECIFICS
#define MAXCOLUMNS 19
#define MAXROWS 15
#define TOTALNODES 285 // 19 * 15
#define MAXFOODITEMS 5

//* ========================================
// MISC
#define MAX_TURN_COUNT 650

//* ========================================
// PWM
#define PWM_VALUE_STRAIGHT 190
#define PWM_VALUE_SLOW 180
#define PWM_VALUE_STOP 128
#define PWM_VALUE_VEER 175
#define PWM_VALUE_90_FORWARD 185
#define PWM_VALUE_90_REVERSE 70
