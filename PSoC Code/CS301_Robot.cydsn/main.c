#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <project.h>

#include "defines.h"
#include "globals.h"
#include "functions.h"

volatile uint8_t startTurnFlag = 0;
volatile uint8_t uTurnFlag = 0;
volatile uint8_t finishTurnFlag = 0;

volatile int turnOrder[MAX_TURN_COUNT] = { -1 };
volatile int turnIndex = -1;
volatile int turnElement = 0;

volatile int count = 0;
volatile int firstCount = 0, secondCount = 0, thirdCount = 0, fourthCount = 0, fifthCount = 0;
volatile int newFoodList[MAXFOODITEMS][3] = {{0}};

/*
CY_ISR(ONADCONVERTCOMPLETE):
    Interrupt logic to be executed when ADC finishes a conversion.
*/
CY_ISR(onADConvertComplete) {
    // Storing the results of the ADC conversions into the ADC struct.
	valueADC.rCentre = ADC_GetResult16(2);
    valueADC.rRear = ADC_GetResult16(3);
    valueADC.rRightCentre = ADC_GetResult16(4);
    valueADC.rLeftCentre = ADC_GetResult16(5);
    valueADC.rLeftTop = ADC_GetResult16(6);
    valueADC.rRightTop = ADC_GetResult16(7);
    
    // Setting the results flag and turning off the ADC to process the results before starting the ADc again.
    valueADC.resultFlag = 1;
    ADC_StopConvert();
}

/*
CY_ISR(ONTIMEOUT):
    Interrupt logic to be executed when Timer_NewTurn times out.
*/
CY_ISR(onTimeout) {
	finishTurnFlag = 0;
    Timer_NewTurn_Stop();
    LED_OFF;
}

/*
INT MAIN():
    Main program that is run on programming the PSoC. The code begins to execute from here.
*/
int main() {
    
    /*
    INITIALISATION:
        This section of the main function will only run once. Here, we initialise variables, and component/isr configurations.
    */
    ADC_Start(); // Starts the ADC component.
    ADC_StartConvert(); // Starts the ADC conversions.
    isr_ADC_StartEx(onADConvertComplete); // Starts the ISR component, connected to ADC. When ADC finishes a conversion, it should execute the interrupt logic written in the "onADConvertComplete" ISR.
    
    PWM_1_Start(); // Starts the PWM_1 component for the left motor.
	PWM_2_Start(); // Starts the PWM_2 component for the right motor.
    QuadDec_M1_Start(); // Starts the QuadDec_M1 component.
    QuadDec_M2_Start(); // Starts the QuadDec_M2 component.
    
	//UART_Start(); // Starts the UART component. Uncomment to see the order of priority for the turns the robot needs to make.
    isr_NewTurn_StartEx(onTimeout); // Starts the ISR component, connected to Timer_NewTurn. When Timer_NewTurn overflows, it should execute the interrupt logic written in the "onTimeout" ISR.
    
    /*
    LEVEL SELECTION:
        This section of the main function will wait for the switch input to determine which level of play the robot is currently in.
        The LED wil stay on until a level is chosen, to indicate that the robot is waiting for an input. 
    */
    LED_ON;
    while(1) {
        if(SW_1_Read() == 0) {
            levelOne();
            break;
        } else if(SW_2_Read() == 0) {
            levelTwo();
            break;
        }
    }
    LED_OFF;
    
    CyDelay(3000); // Small delay after choosing the level.
    CYGlobalIntEnable; // Enables interrupts.
    
    /*
    CONTINUOUS:
        This section of the main function will continuously run.
    */
    while(1) {
        
        /*
        ADC CALCULATIONS:
            This section of code checks the ADC result flag that is set high when the ADC has completed one set of conversions. The ADC is stopped when it reaches this stage.
            It calls the function that processes these direct ADC results.
            If the ADC has completed the maximum number of conversions for one conversion cycle, then we call the function that sets the status of each sensor.
            Once these functions have finished running, the ADC is started up again.
        */
        if(valueADC.resultFlag == 1) {
            processResultsADC();
            ADC_StartConvert();
        }
        
        /*
        ROBOT MOVEMENT:
            This section of the main function will causes the robot to move by calling functions that change the PWM values of the motors.
            It calls a function that returns the direction that we desire to move in, and based on this return value, we call the corresponding movement function.
        */
        switch(determineDirection()) {
            case STRAIGHT:
				goStraight();
                break;
			case SLOW:
				goSlow();
                break;
            case STOP:
				goStop();
                break;
			case LEFT_VEER:
				goLeftVeer();
                break;
			case RIGHT_VEER:
				goRightVeer();
                break;
			case LEFT_90:
				goLeft90();
                break;
			case RIGHT_90:
				goRight90();
                break;
			default:
				goStop();
                break;
        }
    }
    
    return 0;
}