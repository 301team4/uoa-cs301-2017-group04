#ifndef GLOBALS_H
#define GLOBALS_H

#include <project.h>
#include "defines.h"

//* ========================================
// ENUMS
/*
    ENUM GODIRECTION: Stores the map-specific/top-down directions in order of priority from highest to lowest.
        ORDER: When starting from...
            TOP LEFT(1,1): LDRU
            TOP RIGHT(17,1): RULD/RLUD
            BOT LEFT(1,13): RLDU
            BOT RIGHT(17,13): LRDU/RLDU
            CENTRE(9,6): LRUD/URDL/URLD
    ENUM GOMOVEMENT: Stores the robot-specific movements for the robot to take.
*/
enum goDirection{LEFT, RIGHT, DOWN, UP};
enum goMovement{STRAIGHT, SLOW, STOP, UTURN, LEFT_VEER, RIGHT_VEER, LEFT_90, RIGHT_90};

//* ========================================
// GLOBALS
/*
    VOLATILE UINT8_T STARTTURNFLAG: Flag to determine the stages of a 90 degree turn.
    VOLATILE UINT8_T UTURNFLAG: Flag to determine the stages of a U-turn.
    VOLATILE UINT8_T FINISHTURNFLAG: Flag to determine the delay between accepting a new turn.
*/
extern volatile uint8_t startTurnFlag;
extern volatile uint8_t uTurnFlag;
extern volatile uint8_t finishTurnFlag;

/*
    VOLATILE INT TURNORDER[MAX_TURN_COUNT]: Array to store the order of turns the robot needs to make.
    VOLATILE INT TURNINDEX: Index to store the size of the turnorder array.
    VOLATILE INT TURNELEMENT: Index to store the current element of the turn array.
*/
extern volatile int turnOrder[MAX_TURN_COUNT];
extern volatile int turnIndex;
extern volatile int turnElement;

/*
    VOLATILE INT COUNT: Holds the total number of steps we've made on any given path.
    VOLATILE INT _____COUNT: These five count variables hold the number of steps for the current path. Since there are only five food items, we only need five specific count variables.
    VOLATILE INT NEWFOODLIST[MAXFOODITEMS][3]: This is a 2d array which holds all the food items from the given food list but also their visit status. This array is 5 rows by 3 columns.
*/
extern volatile int count;
extern volatile int firstCount, secondCount, thirdCount, fourthCount, fifthCount;
extern volatile int newFoodList[MAXFOODITEMS][3];

//* ========================================
// STRUCTURES

/*
    STRUCT STRUCTADC VALUEADC: Holds the relevant data for ADC conversions/values.
        VOLATILE UINT16_T R_____: Stores the direct ADC result for the corresponding sensor.
        VOLATILE UINT16_T MIN_____: Stores the minimum ADC result for the respective set of data.
        VOLATILE UINT16_T MAX_____: Stores the maximum ADC result for the respective set of data.
        VOLATILE UINT16_T S_____: Stores the status for the corresponding sensor; 0 = Off-track, 1 = On-track.
        VOLATILE UINT8_T CONVCOUNT: Stores the counter for the respective set of data.
        VOLATILE UINT8_T RESULTFLAG: Flag to determine when to process the ADC results.
*/
struct structADC {
    volatile uint16_t rCentre, rRear, rRightCentre, rLeftCentre, rLeftTop, rRightTop; // RESULTS
    volatile uint16_t minCentre, minRear, minRightCentre, minLeftCentre, minLeftTop, minRightTop; // MIN
	volatile uint16_t maxCentre, maxRear, maxRightCentre, maxLeftCentre, maxLeftTop, maxRightTop; // MAX
    volatile uint16_t sCentre, sRear, sRightCentre, sLeftCentre, sLeftTop, sRightTop; // ON/OFF LINE
	
	volatile uint8_t convCount;
	volatile uint8_t resultFlag;
}valueADC;

/*
    STRUCT LIST PATH[TOTALNODES], _____PATH[TOTALNODES]: The structure that holds all the path information. It is initialised to a path array consisting of a TOTALNODES elements.
	    INT X/Y: x and y specify the x and y coordinates of the path at any given element in the path array.
		INT F: f specifes the total cost of this path.
*/
struct list {
	int x, y;
	int f;
}path[TOTALNODES], firstPath[TOTALNODES], secondPath[TOTALNODES], thirdPath[TOTALNODES], fourthPath[TOTALNODES], fifthPath[TOTALNODES];

/*
    STRUCT NODE NODE[MAXCOLUMNS][MAXROWS]: The structure that holds all the node information. It is initialised to a node 2d array consisiting of MACOLUMNS x MAXROWS.
		   INT WALKABLE: walkable specifies if the current node is blocked by a wall or if it is open to walk on.
		   INT OPENNODE: openNode specifies a node that we are considering.
		   INT CLOSEDNODE: closedNode specifies a node that we have already travelled on.
		   INT F: f specifies the total cost of the node (g + h).
		   INT G: g specifies the cost from the parent node to the node we are considering.
		   INT H: h specifies the cost from the node we are considering to the end goal.
		   INT PARENTX/PARENTY: parentX and parentY specify the previous position/coordinates we were at.
*/
struct NODE {
	int walkable;
	int openNode, closedNode;
	int f, g, h;
	int parentX, parentY;
}node[MAXCOLUMNS][MAXROWS];

#endif
