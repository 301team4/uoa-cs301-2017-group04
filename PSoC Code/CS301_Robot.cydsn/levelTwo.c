#include <stdio.h>
#include <stdlib.h>
#include <project.h>

#include "globals.h"
#include "defines.h"
#include "functions.h"
#include "map.h"

/*
VOID INITNODES(VOID):
    No input or output.
    Loops through the maze based on MAXROWS and MAXCOLUMNS.
	Sets all characteristics of the node to 0 (initialise).
	Checks the maze to see if the node is walkable or not (0 being walkable, 1 being not walkable).
*/
void initNodes(void) {
	int x = 0, y = 0;

	for (x = 0; x < MAXCOLUMNS; x++) {				// Loop through the columns
		for (y = 0; y < MAXROWS; y++) {				// Loop through the rows
			node[x][y].walkable = map[y][x];		// Check to see if node is walkable or not
			node[x][y].openNode = 0;
			node[x][y].closedNode = 0;
			node[x][y].f = 0;
			node[x][y].g = 0;
			node[x][y].h = 0;
			node[x][y].parentX = 0;
			node[x][y].parentY = 0;
		}
	}
}

/*
STRUCT LIST *BESTPATH(INT STARTX, INT STARTY, INT ENDX, INT ENDY):
    Takes in the current position, and the end position.
	Calculates f, g and h - modifies path and node characteristics accordingly.
	Returns a path of type struct list.
*/
struct list *bestPath(int startX, int startY, int endX, int endY) {
	int x = 0, y = 0;
	int dx = 0, dy = 0;
	int cx = startX, cy = startY;									// Set our current location to the start location

	int lowestFCost = 1000;											// Define an initial lowest f cost as a check variable

	node[startX][startY].openNode = 1;								// Set the start location as an open node
	node[startX][startY].closedNode = 0;

	while (cx != endX || cy != endY) {								// Keep performing the algorithm until our current location matches the end location
		lowestFCost = 1000;											// Re-initalise our check variable for f cost
		for (x = 0; x < MAXCOLUMNS; x++) {
			for (y = 0; y < MAXROWS; y++) {
				node[x][y].f = node[x][y].g + node[x][y].h;			// Find the current nodes f cost

				if (node[x][y].openNode) {							// If the current node is open, i.e we are considering it
					if (node[x][y].f < lowestFCost) {				// If the current nodes f cost is lower than the check variable f cost
						cx = x;										// Set our current location to the location of the node.
						cy = y;
						lowestFCost = node[x][y].f;					// Update our lowest f cost, with the nodes f cost.
					}												// Repeat for loop until all nodes in the maze are scanned.
				}
			}
		}

		node[cx][cy].openNode = 0;									// The node from the for loop is no longer open.
		node[cx][cy].closedNode = 1;								// The node is now closed as we have travelled on it.

		for (dx = -1; dx <= 1; dx++) {								// dx and dy both go from -1 to 1. So they search all 8 possible
			for (dy = -1; dy <= 1; dy++) {							// directions around the node.
				if ((dx != 0) || (dy != 0)) {
					if ((cx + dx) < MAXCOLUMNS && (cx + dx) > -1 && (cy + dy) < MAXROWS && (cy + dy) > -1) {				// Check to see if what we want to look at is still in bounds
						if (node[cx + dx][cy + dy].walkable == 0 && node[cx + dx][cy + dy].closedNode == 0) {				// Check to see if the node we want to look at is walkable and not closed
							if (node[cx + dx][cy + dy].openNode == 0) {														// Check to see if the node we want to look at is not on the open list
								node[cx + dx][cy + dy].openNode = 1;														// Make it an open node
								node[cx + dx][cy + dy].closedNode = 0;

								node[cx + dx][cy + dy].parentX = cx;														// Set its parent x and y to our current location
								node[cx + dx][cy + dy].parentY = cy;

								if (dx == 0 || dy == 0) {																	// If it is a linear node i.e up, right, down, left; then the cost is just 10
									node[cx + dx][cy + dy].g = 10;															// Set the nodes g cost to 10.
								} else {																					// Else its a diagonal node.
									node[cx + dx][cy + dy].g = 1000;														// We don't want to deal with diagonals, so if it is a diagonal node, set its g cost very high
								}

								node[cx + dx][cy + dy].h = (abs(endX - (cx + dx)) + abs(endY - (cy + dy))) * 10;			// Find h cost using the Manhattan Distance formula
								node[cx + dx][cy + dy].f = node[cx + dx][cy + dy].g + node[cx + dx][cy + dy].h;				// Find the f cost (g + h).


							} else if (node[cx + dx][cy + dy].closedNode == 0 && node[cx + dx][cy + dy].openNode == 1) {	// If it was already open
								if (dx == 0 || dy == 0) {																	// If it is not a diagonal node
									if (node[cx + dx][cy + dy].g == 1000) {													// If it was previously a diagonal node
										node[cx + dx][cy + dy].g = 10;														// Set its g cost to the cost of a linear node

										node[cx + dx][cy + dy].parentX = cx;												// Set its parent x and y to our current location
										node[cx + dx][cy + dy].parentY = cy;

										node[cx + dx][cy + dy].h = (abs(endX - (cx + dx)) + abs(endY - (cy + dy))) * 10;	// Find h cost using the Manhattand Distance formula.
										node[cx + dx][cy + dy].f = node[cx + dx][cy + dy].g + node[cx + dx][cy + dy].h;		// Find the f cost (g + h).
									}
								}
							}
						}
					}
				}
			}
		}
	}

	/*
		We now have all the information needed to draw a path. We retrace our steps
		from the end position to the start position.
	*/

	count = 0;									// Initilize our step count variable to 0

	cx = endX; cy = endY;						// Set our current location to our end location

	while (cx != startX || cy != startY) {		// Keep looping until we have reached the start location that was specified
		path[count].x = node[cx][cy].parentX;	// Find the parent x and y of the current location and store it in the path x and y locations
		path[count].y = node[cx][cy].parentY;
		path[count].f = node[cx][cy].f;

		cx = path[count].x;						// Set the current location the parent x and y locations
		cy = path[count].y;

		count++;								// Incrememnt our step count
	}

	initNodes();								// Re-initialise all the nodes to get rid of any unnecessary values

	return path;								// Return the final path.
}

/*
VOID CALCULATEPATHS(VOID):
    No input or output.
    Loops through the newFoodList and checks with food items are the closest.
    It then orders these from the closes to the farthest away.
    Stores the specific paths into the path variables above.
*/
void calculatePaths(void) {
	int i = 0, j = 0, bestCount = 1000, index = 0;
	int startX = 1, startY = 1, endX = 0, endY = 0;
	struct list *findPath;

	for (i = 0; i < MAXFOODITEMS; i++) {														// Loop as many times as there are food items.
		bestCount = 1000;																		// Set count check variable to a large number.
		for (j = 0; j < MAXFOODITEMS; j++) {													// Loop as many times as there are food items.
			if (newFoodList[j][2] == 0) {														// Check to see if the food item has already been visited.
				findPath = bestPath(startX, startY, newFoodList[j][0], newFoodList[j][1]);		// If it hasn't then find the best path to that item.
				if (count <= bestCount) {														// If the cost of that path is less than the check variable,
					bestCount = count;															// then update the check variable to now hold the new cost
					endX = newFoodList[j][0];													// Set end x and y to the x and y of the food item
					endY = newFoodList[j][1];
					index = j;																	// Hold the index of the food item in the 2d array.
				}
			}
		}

		switch (i) {																			// Switch based on value of i
		case 0:																					// Case 0: First Path
			findPath = bestPath(startX, startY, endX, endY);									// Find the best path
			firstCount = count;																	// Hold the step count value

			for (j = firstCount - 1; j >= 0; j--) {
				firstPath[j].x = path[j].x;
				firstPath[j].y = path[j].y;
			}

			startX = endX;																		// Set the new starting position to the food item/current location
			startY = endY;
			newFoodList[index][2] = 1;															// Mark the food item as visited.
			break;
		case 1:																					// Case 1: Second Path
			findPath = bestPath(startX, startY, endX, endY);
			secondCount = count;

			for (j = secondCount - 1; j >= 0; j--) {
				secondPath[j].x = path[j].x;
				secondPath[j].y = path[j].y;
			}

			startX = endX;
			startY = endY;
			newFoodList[index][2] = 1;
			break;
		case 2:																					// Case 2: Third Path
			findPath = bestPath(startX, startY, endX, endY);
			thirdCount = count;

			for (j = thirdCount - 1; j >= 0; j--) {
				thirdPath[j].x = path[j].x;
				thirdPath[j].y = path[j].y;
			}

			startX = endX;
			startY = endY;
			newFoodList[index][2] = 1;
			break;
		case 3:																					// Case 3: Fourth Path
			findPath = bestPath(startX, startY, endX, endY);
			fourthCount = count;

			for (j = fourthCount - 1; j >= 0; j--) {
				fourthPath[j].x = path[j].x;
				fourthPath[j].y = path[j].y;
			}

			startX = endX;
			startY = endY;
			newFoodList[index][2] = 1;
			break;
		case 4:																					// Case 4: Fifth Path
			findPath = bestPath(startX, startY, endX, endY);
			fifthCount = count;

			for (j = fifthCount - 1; j >= 0; j--) {
				fifthPath[j].x = path[j].x;
				fifthPath[j].y = path[j].y;
			}

			startX = endX;
			startY = endY;
			newFoodList[index][2] = 1;
			break;
		default:
			break;
		}
	}
}

/*
VOID LEVELTWO(VOID):
    Function to call to return the path needed to traverse the maze and collect all pellets. This follows the A* algorithm.
*/
void levelTwo(void) {
	int i = 0;

	initNodes();													// Initialise all the nodes

	for (i = 0; i < MAXFOODITEMS; i++) {							// Loop as many times as there are food items
		newFoodList[i][0] = food_list[i][0];						// Load the food item coordinates into our new food list 2d array
		newFoodList[i][1] = food_list[i][1];
		newFoodList[i][2] = 0;										// Set all food items to unvisited.
	}

	calculatePaths();												// Find the paths in the order that we have to visit the food items

    // Array and Index to store the complete path needed to reach all pellets.
	int pathOrder[MAX_TURN_COUNT][2];
	int pathIndex = 0;

    // Assigning values to the main path array.
	for (i = firstCount - 1; i >= 0; i--) {
		pathOrder[pathIndex][0] = firstPath[i].x;
		pathOrder[pathIndex++][1] = firstPath[i].y;
	}
	for (i = secondCount - 1; i >= 0; i--) {
		pathOrder[pathIndex][0] = secondPath[i].x;
		pathOrder[pathIndex++][1] = secondPath[i].y;
	}
	for (i = thirdCount - 1; i >= 0; i--) {
		pathOrder[pathIndex][0] = thirdPath[i].x;
		pathOrder[pathIndex++][1] = thirdPath[i].y;
	}
	for (i = fourthCount - 1; i >= 0; i--) {
		pathOrder[pathIndex][0] = fourthPath[i].x;
		pathOrder[pathIndex++][1] = fourthPath[i].y;
	}
	for (i = fifthCount - 1; i >= 0; i--) {
		pathOrder[pathIndex][0] = fifthPath[i].x;
		pathOrder[pathIndex++][1] = fifthPath[i].y;
	}

    // Converting the coordinates into a list of map-specific directions to move the robot at each potential turn.
	int priorityOrder[MAX_TURN_COUNT];
	int priorityIndex = 0;
	int currentDirection = -1;
	int newDirection = -1;

    // Initialising the priority order array based on the current direction.
	if(pathOrder[1][0] < pathOrder[0][0]) {
		currentDirection = LEFT;
	} else if(pathOrder[1][0] > pathOrder[0][0]) {
		currentDirection = RIGHT;
	} else if(pathOrder[1][1] < pathOrder[0][1]) {
		currentDirection = UP;
	} else {
		currentDirection = DOWN;
	}
	priorityOrder[priorityIndex++] = currentDirection;

    // Setting the remaining values of the path order array.
	for(i = 2; i < pathIndex; i++) {
        if(pathOrder[i][0] < pathOrder[i-1][0]) {
			newDirection = LEFT;
		} else if(pathOrder[i][0] > pathOrder[i-1][0]) {
			newDirection = RIGHT;
		} else if(pathOrder[i][1] < pathOrder[i-1][1]) {
			newDirection = UP;
		} else {
			newDirection = DOWN;
		}

		if(currentDirection != newDirection) { // If there has been a change in direction, record it in the path array.
			currentDirection = newDirection;
			priorityOrder[priorityIndex++] = currentDirection;
		} else {
            // If the current direction is horizontal, and it is possible to travel vertically, it is an intersection. Record the current direction.
			if((currentDirection == LEFT) || (currentDirection == RIGHT)) {
				if((map[pathOrder[i-1][1] - 1][pathOrder[i-1][0]] == 0) || (map[pathOrder[i-1][1] + 1][pathOrder[i-1][0]] == 0)) {
					priorityOrder[priorityIndex++] = currentDirection;
				}
			} else { // If the current direction is vertical, and it is possible to travel horizontally, it is an intersection. Record the current direction.
				if((map[pathOrder[i-1][1]][pathOrder[i-1][0] - 1] == 0) || (map[pathOrder[i-1][1]][pathOrder[i-1][0] + 1] == 0)) {
					priorityOrder[priorityIndex++] = currentDirection;
				}
			}
		}
	}

    // Storing robot-specific directions within the global priority array to traverse the maze.
    turnIndex = priorityIndex - 1;
	for(i = 1; i < priorityIndex; i++) {
		turnOrder[i-1] = mapToRobotTurn(priorityOrder[i-1],priorityOrder[i]);
        
        /*
        PRINTING ROBOT-SPECIFIC DIRECTIONS:
            If UART is turned on, it is possible to view the list of turns the robot will make by using a terminal program. Uncomment to view.
        */
//        char str[50] = {0};
//        switch(turnOrder[i-1]) {
//			case STRAIGHT:
//				sprintf(str, "%03d:\tSTRAIGHT\n", i-1); UART_PutString(str); break;
//			case LEFT_90:
//				sprintf(str, "%03d:\tLEFT\n", i-1); UART_PutString(str); break;
//			case RIGHT_90:
//				sprintf(str, "%03d:\tRIGHT\n", i-1); UART_PutString(str); break;
//			case UTURN:
//				sprintf(str, "%03d:\tUTURN\n", i-1); UART_PutString(str); break;
//			default:
//				sprintf(str, "%03d:\tNOT GOOD\n", i-1); UART_PutString(str); break;
//		}
	}
}