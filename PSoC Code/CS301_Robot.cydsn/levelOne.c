#include <stdio.h>
#include <project.h>

#include "globals.h"
#include "defines.h"
#include "functions.h"
#include "map.h"

/*
INT MAPTOROBOTTURN(INT PREVORIENTATION, INT CURRENTORIENTATION):
    Function to return the robot-specific direction to travel in.
*/
int mapToRobotTurn(int prevOrientation, int currentOrientation) {
	switch(prevOrientation) {
		case DOWN: // If we were travelling downw map-specific...
			switch(currentOrientation) {
				case RIGHT: // And we now move right map-specific...
					return LEFT_90;
				case DOWN: // And we now move down map-specific...
					return STRAIGHT;
				case LEFT: // And we now move left map-specific...
					return RIGHT_90;
				case UP: // And we now move up map-specific...
					return UTURN;
			}
			break;
		case RIGHT:
			switch(currentOrientation) {
				case RIGHT: // And we now move right map-specific...
					return STRAIGHT;
				case DOWN: // And we now move down map-specific...
					return RIGHT_90;
				case LEFT: // And we now move left map-specific...
					return UTURN;
				case UP: // And we now move up map-specific...
					return LEFT_90;
			}
			break;
		case LEFT:
			switch(currentOrientation) {
				case RIGHT: // And we now move right map-specific...
					return UTURN;
				case DOWN: // And we now move down map-specific...
					return LEFT_90;
				case LEFT: // And we now move left map-specific...
					return STRAIGHT;
				case UP: // And we now move up map-specific...
					return RIGHT_90;
			}
			break;
		case UP:
			switch(currentOrientation) {
				case RIGHT: // And we now move right map-specific...
					return RIGHT_90;
				case DOWN: // And we now move down map-specific...
					return UTURN;
				case LEFT: // And we now move left map-specific...
					return LEFT_90;
				case UP: // And we now move up map-specific...
					return STRAIGHT;
			}
			break;
	}
	return STRAIGHT; // If nothing is valid, move to straight ahead.
}

/*
VOID LEVELONE():
    Function to call to return the path needed to traverse the maze and collect all pellets. This follows the DFS algorithm, adjusted to travel everywhere.
*/
void levelOne() {
    int mapVisited[15][19];
    
    int currentPosition[2];
    currentPosition[0] = INITIAL_Y;
    currentPosition[1] = INITIAL_X;
	int currentDirection = INITIAL_DIRECTION;

	int priorityOrder[MAX_TURN_COUNT] = { -1 };
	int priorityIndex = 0;

    // Creating a copy of the map data to store the number of times each square has been visited.
	if(1) {
		int i = 0;
		for(; i < 15; i++) {
			int j = 0;
            for(; j < 19; j++) {
				if(map[i][j] == 1) {
					mapVisited[i][j] = -1;
				} else {
					mapVisited[i][j] = 0;
                }
			}
		}
	}

	mapVisited[currentPosition[0]][currentPosition[1]]++;
	priorityOrder[priorityIndex++] = currentDirection;

	int finishedMap = 0;
	while(!finishedMap) { // Until the robot have visited all squares of the map...

		if(1) {
			int oldDirection = currentDirection;
			int intersectionPaths = 0;

			if(1) {
                // Checking to see if there is a new path to travel down, in order of preference.
				int index = 3;
				for(; index >= 0; index--) {
					switch(index) {
						case DOWN:
							if(mapVisited[currentPosition[0]+1][currentPosition[1]] == 0) {
								currentDirection = DOWN; intersectionPaths++;
							}
							break;
						case RIGHT:
							if(mapVisited[currentPosition[0]][currentPosition[1]+1] == 0) {
								currentDirection = RIGHT; intersectionPaths++;
							}
							break;
						case LEFT:
							if(mapVisited[currentPosition[0]][currentPosition[1]-1] == 0) {
								currentDirection = LEFT; intersectionPaths++;
							}
							break;
						case UP:
							if(mapVisited[currentPosition[0]-1][currentPosition[1]] == 0) {
								currentDirection = UP; intersectionPaths++;
							}
							break;
					}
				}
			}

			if((currentDirection != oldDirection) || (intersectionPaths > 1)) { // If the robot is at an intersection, or changed direction...
				priorityOrder[priorityIndex++] = currentDirection;
			} else if(intersectionPaths == 0) { // If the robot is not at an intersection at all...

                // If the robot is at a dead end...
				int uTurning = 0;
				switch(currentDirection) {
					case DOWN:
						if((mapVisited[currentPosition[0]+1][currentPosition[1]] == -1) && (mapVisited[currentPosition[0]][currentPosition[1]-1] == -1) && (mapVisited[currentPosition[0]][currentPosition[1]+1] == -1)) {
							currentDirection = UP; uTurning++;
						}
						break;
					case RIGHT:
						if((mapVisited[currentPosition[0]][currentPosition[1]+1] == -1) && (mapVisited[currentPosition[0]-1][currentPosition[1]] == -1) && (mapVisited[currentPosition[0]+1][currentPosition[1]] == -1)) {
							currentDirection = LEFT; uTurning++;
						}
						break;
					case LEFT:
						if((mapVisited[currentPosition[0]][currentPosition[1]-1] == -1) && (mapVisited[currentPosition[0]-1][currentPosition[1]] == -1) && (mapVisited[currentPosition[0]+1][currentPosition[1]] == -1)) {
							currentDirection = RIGHT; uTurning++;
						}
						break;
					case UP:
						if((mapVisited[currentPosition[0]-1][currentPosition[1]] == -1) && (mapVisited[currentPosition[0]][currentPosition[1]-1] == -1) && (mapVisited[currentPosition[0]][currentPosition[1]+1] == -1)) {
							currentDirection = DOWN; uTurning++;
						}
						break;
					default:
						break;
				}

				if(uTurning != 0) { // Since the robot is performing a uturn, record this in the array.
					priorityOrder[priorityIndex++] = currentDirection;
				} else { // If we are not at an intersection, nor are we at a dead-end...

					int potentialDirection = -1;
					int minFlag = 10000;

					if(1) {
						// If we are travelling along visited paths horizontally, and come across a path vertically, that is an intersection.
                        if((mapVisited[currentPosition[0]-1][currentPosition[1]] != -1) || (mapVisited[currentPosition[0]+1][currentPosition[1]] != -1)) {
							if(mapVisited[currentPosition[0]][currentPosition[1]+1] != -1) {
								intersectionPaths++;
							}
							if(mapVisited[currentPosition[0]][currentPosition[1]-1] != -1) {
								intersectionPaths++;
							}
						}
                        // If we are travelling along visited paths vertically, and come across a path horizontally, that is an intersection.
						if((mapVisited[currentPosition[0]][currentPosition[1]-1] != -1) || (mapVisited[currentPosition[0]][currentPosition[1]+1] != -1)) {
							if(mapVisited[currentPosition[0]+1][currentPosition[1]] != -1) {
								intersectionPaths++;
							}
							if(mapVisited[currentPosition[0]-1][currentPosition[1]] != -1) {
								intersectionPaths++;
							}
						}
					}

					if(1) {
                        // Checking to see if there is a visited path to travel down, in order of preference.
						int index = 3;
						for(; index >= 0; index--) {
							switch(index) {
								case DOWN:
									if((mapVisited[currentPosition[0]+1][currentPosition[1]] <= minFlag) && (mapVisited[currentPosition[0]+1][currentPosition[1]] != -1) && (currentDirection != UP)) {
										minFlag = mapVisited[currentPosition[0]+1][currentPosition[1]];
										potentialDirection = DOWN;
									}
									break;
								case RIGHT:
									if((mapVisited[currentPosition[0]][currentPosition[1]+1] <= minFlag) && (mapVisited[currentPosition[0]][currentPosition[1]+1] != -1) && (currentDirection != LEFT)) {
										minFlag = mapVisited[currentPosition[0]][currentPosition[1]+1];
										potentialDirection = RIGHT;
									}
									break;
								case LEFT:
									if((mapVisited[currentPosition[0]][currentPosition[1]-1] <= minFlag) && (mapVisited[currentPosition[0]][currentPosition[1]-1] != -1) && (currentDirection != RIGHT)) {
										minFlag = mapVisited[currentPosition[0]][currentPosition[1]-1];
										potentialDirection = LEFT;
									}
									break;
								case UP:
									if((mapVisited[currentPosition[0]-1][currentPosition[1]] <= minFlag) && (mapVisited[currentPosition[0]-1][currentPosition[1]] != -1) && (currentDirection != DOWN)) {
										minFlag = mapVisited[currentPosition[0]-1][currentPosition[1]];
										potentialDirection = UP;
									}
									break;
							}
						}
					}

					if((potentialDirection != oldDirection) && (potentialDirection != -1)) {
						currentDirection = potentialDirection;
						priorityOrder[priorityIndex++] = currentDirection;
					} else if(intersectionPaths != 0) {
						priorityOrder[priorityIndex++] = currentDirection;
					}

				}
			}
		}

        // Moving the robot based on its current direction.
		switch(currentDirection) {
			case DOWN:
				mapVisited[++currentPosition[0]][currentPosition[1]]++;
				break;
			case RIGHT:
				mapVisited[currentPosition[0]][++currentPosition[1]]++;
				break;
			case LEFT:
				mapVisited[currentPosition[0]][--currentPosition[1]]++;
				break;
			case UP:
				mapVisited[--currentPosition[0]][currentPosition[1]]++;
				break;
			default:
				break;
		}

        // Checking to see if the robot has visited every square of the map.
		if(1) {
			finishedMap = 1;
			int i = 0;
            for(; i < 15; i++) {
				int j = 0;
                for(; j < 19; j++) {
					if(mapVisited[i][j] == 0) {
						finishedMap = 0;
						break;
					}
				}
				if(finishedMap == 0) {
					break;
				}
			}
		}

	}

	if(1) {
        
        // Storing robot-specific directions within the global priority array to traverse the maze.
        int i = 1;
        turnIndex = priorityIndex - 1;
        for(; i < priorityIndex; i++) {
            turnOrder[i-1] = mapToRobotTurn(priorityOrder[i-1],priorityOrder[i]);
            
            /*
            PRINTING ROBOT-SPECIFIC DIRECTIONS:
                If UART is turned on, it is possible to view the list of turns the robot will make by using a terminal program. Uncomment to view.
            */
//            char str[50] = {0};
//            switch(turnOrder[i-1]) {
//				case STRAIGHT:
//					sprintf(str, "%03d:\tSTRAIGHT\n", i-1); UART_PutString(str); break;
//				case LEFT_90:
//					sprintf(str, "%03d:\tLEFT\n", i-1); UART_PutString(str); break;
//				case RIGHT_90:
//					sprintf(str, "%03d:\tRIGHT\n", i-1); UART_PutString(str); break;
//				case UTURN:
//					sprintf(str, "%03d:\tUTURN\n", i-1); UART_PutString(str); break;
//				default:
//					sprintf(str, "%03d:\tNOT GOOD\n", i-1); UART_PutString(str); break;
//			}
        }

    }
}