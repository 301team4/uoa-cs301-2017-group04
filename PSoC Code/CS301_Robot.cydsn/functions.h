void processResultsADC(void);
void processSensorsADC(void);

int mapToRobotTurn(int prevOrientation, int currentOrientation);
int determineDirection(void);
void levelOne(void);
void levelTwo(void);

void goStraight(void);
void goSlow(void);
void goStop(void);
void goLeftVeer(void);
void goRightVeer(void);
void goLeft90(void);
void goRight90(void);