#include <project.h>

#include "globals.h"
#include "functions.h"
#include "defines.h"

/*
VOID PROCESSRESULTSADC():
    Function to process the results of one set of ADC conversions.
*/
void processResultsADC() {
    /*
    INITIAL SET:
        If the results are the first set of values for the current data set, then set both the min and max for all sensors as the results.
    */
    if(valueADC.convCount == 0) {
        valueADC.minCentre = valueADC.rCentre;
        valueADC.maxCentre = valueADC.rCentre;
        
        valueADC.minRear = valueADC.rRear;
        valueADC.maxRear = valueADC.rRear;
        
        valueADC.minRightCentre = valueADC.rRightCentre;
        valueADC.maxRightCentre = valueADC.rRightCentre;
        
        valueADC.minLeftCentre = valueADC.rLeftCentre;
        valueADC.maxLeftCentre = valueADC.rLeftCentre;
        
        valueADC.minLeftTop = valueADC.rLeftTop;
        valueADC.maxLeftTop = valueADC.rLeftTop;
        
        valueADC.minRightTop = valueADC.rRightTop;
        valueADC.maxRightTop = valueADC.rRightTop;
    } else {
    /*
    RESULT PROCESSING:
        If the results for the sensors are lower than the current minimum, or greater than the current maximum, update the respective values.
    */
        if(valueADC.rCentre < valueADC.minCentre) {
            valueADC.minCentre = valueADC.rCentre;
        } else if(valueADC.rCentre > valueADC.maxCentre) {
            valueADC.maxCentre = valueADC.rCentre;
        }
        if(valueADC.rRear < valueADC.minRear) {
            valueADC.minRear = valueADC.rRear;
        } else if(valueADC.rRear > valueADC.maxRear) {
            valueADC.maxRear = valueADC.rRear;
        }
        if(valueADC.rRightCentre < valueADC.minRightCentre) {
            valueADC.minRightCentre = valueADC.rRightCentre;
        } else if(valueADC.rRightCentre > valueADC.maxRightCentre) {
            valueADC.maxRightCentre = valueADC.rRightCentre;
        }
        if(valueADC.rLeftCentre < valueADC.minLeftCentre) {
            valueADC.minLeftCentre = valueADC.rLeftCentre;
        } else if(valueADC.rLeftCentre > valueADC.maxLeftCentre) {
            valueADC.maxLeftCentre = valueADC.rLeftCentre;
        }
        if(valueADC.rLeftTop < valueADC.minLeftTop) {
            valueADC.minLeftTop = valueADC.rLeftTop;
        } else if(valueADC.rLeftTop > valueADC.maxLeftTop) {
            valueADC.maxLeftTop = valueADC.rLeftTop;
        }
        if(valueADC.rRightTop < valueADC.minRightTop) {
            valueADC.minRightTop = valueADC.rRightTop;
        } else if(valueADC.rRightTop > valueADC.maxRightTop) {
            valueADC.maxRightTop = valueADC.rRightTop;
        }
    }
    
    valueADC.resultFlag = 0; // Reset flag.
    
    /*
    PROCESS FOR SENSORS:
       Increment current result set counter, and check if there are enough samples to process the results to set the status for each sensor.
    */
    valueADC.convCount++;
    if(valueADC.convCount == MAX_ADC_COUNT) {
        processSensorsADC();
    };
}

/*
VOID PROCESSSENSORSADC():
    Function to process the results of the ADC conversions and set the status for the various sensors.
*/
void processSensorsADC() {
    /*
    DIFFERENCE CALCULATIONS:
        To determine whether the sensor is on or off the track, the difference between the minimum and maximum readings needs to be calculated.
    */
    volatile uint16_t diffCentre = valueADC.maxCentre - valueADC.minCentre;
    volatile uint16_t diffRear = valueADC.maxRear - valueADC.minRear;
    volatile uint16_t diffRightCentre = valueADC.maxRightCentre - valueADC.minRightCentre;
    volatile uint16_t diffLeftCentre = valueADC.maxLeftCentre - valueADC.minLeftCentre;
    volatile uint16_t diffLeftTop = valueADC.maxLeftTop - valueADC.minLeftTop;
    volatile uint16_t diffRightTop = valueADC.maxRightTop - valueADC.minRightTop;
       
    /*
    SENSOR ENABLE/DISABLE:
        Setting the status of each sensor based on difference value when compared to threshold voltage values.
    */
    if(diffCentre <= ON_TRACK_THRESHHOLD) {
        valueADC.sCentre = 1;
    } else if(OFF_TRACK_THRESHHOLD <= diffCentre) {
        valueADC.sCentre = 0;
    }
    if(diffRear <= ON_TRACK_THRESHHOLD) {
        valueADC.sRear = 1; 
    } else if(OFF_TRACK_THRESHHOLD <= diffRear) {
        valueADC.sRear = 0; 
    }
    if(diffRightCentre <= ON_TRACK_THRESHHOLD) {
        valueADC.sRightCentre = 1; 
    } else if(OFF_TRACK_THRESHHOLD <= diffRightCentre) {
        valueADC.sRightCentre = 0; 
    }
    if(diffLeftCentre <= ON_TRACK_THRESHHOLD) {
        valueADC.sLeftCentre = 1; 
    } else if(OFF_TRACK_THRESHHOLD <= diffLeftCentre) {
        valueADC.sLeftCentre = 0; 
    }
    if(diffRightTop <= ON_TRACK_THRESHHOLD) {
        valueADC.sRightTop = 1; 
    } else if(OFF_TRACK_THRESHHOLD <= diffRightTop) {
        valueADC.sRightTop = 0; 
    }
    if(diffLeftTop <= ON_TRACK_THRESHHOLD) {
        valueADC.sLeftTop = 1;
    } else if(OFF_TRACK_THRESHHOLD <= diffLeftTop) {
        valueADC.sLeftTop = 0;
    }
}